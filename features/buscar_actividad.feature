Feature: Search activities

  Scenario: Select a category
    Given a list of categories
    When user select a category 
    Then list related activities

  Scenario: Select a location
    Given a list of locations
    When user select a location
    Then list activities located

  Scenario: Select a schedule
    Given a list of available scheludes
    When user select a schedule
    Then list available schedules
  
  

