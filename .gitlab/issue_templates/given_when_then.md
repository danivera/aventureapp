* **GIVEN** (*dado que*): [*especifica el escenario-precondiciones*]

* **WHEN** (*cuando*): [*condiciones de las acciones a ejecutar*]

* **THEN** (*entonces*): [*resultado esperado y validaciones a realizar*]
